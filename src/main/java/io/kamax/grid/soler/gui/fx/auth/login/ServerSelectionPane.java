/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.gui.fx.auth.login;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXTextField;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import org.apache.commons.lang3.StringUtils;

public class ServerSelectionPane extends GridPane {

    private Label errorLabel;
    private Label hostLabel;
    private JFXTextField hostField;
    private JFXButton loginButton;

    public ServerSelectionPane() {
        errorLabel = new Label();
        hostField = new JFXTextField();
        hostLabel = new Label("Server: ");
        hostLabel.setLabelFor(hostField);
        loginButton = new JFXButton("Connect");
        loginButton.setStyle(".gui-button {-gui-button-type: RAISED;\n" +
                "     -fx-background-color: blue;\n" +
                "     -fx-text-fill: white;\n" +
                "}");

        loginButton.setDisable(true);
        hostField.textProperty().addListener((i, o, n) -> loginButton.setDisable(StringUtils.isBlank(n)));

        add(errorLabel, 1, 1, 5, 1);
        addRow(2, hostLabel, hostField);
        add(loginButton, 3, 2, 2, 1);
    }

    public void addListener(EventHandler<ActionEvent> handler) {
        hostField.setOnAction(e -> loginButton.fire());
        loginButton.setOnAction(handler);
    }

    public String getInput() {
        return hostField.getText();
    }

    public void setInput(String input) {
        hostField.setText(input);
    }

    public void showError(String message) {
        errorLabel.setText(message);
    }

    public void clearError() {
        errorLabel.setText(null);
    }

}
