/*
 * Copyright (C) 2019 Kamax Sarl
 *
 * https://www.kamax.io/
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package io.kamax.grid.soler.gui.controler;

import com.google.gson.JsonObject;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXPasswordField;
import com.jfoenix.controls.JFXTextField;
import io.kamax.grid.soler.api.GsonUtil;
import io.kamax.grid.soler.api.SolerClient;
import io.kamax.grid.soler.api.SolerEndpoint;
import io.kamax.grid.soler.api.SolerIdentityClient;
import io.kamax.grid.soler.core.SolerHttpEndpoint;
import io.kamax.grid.soler.core.State;
import io.kamax.grid.soler.core.io.UIAuthJson;
import io.kamax.grid.soler.gui.fx.auth.login.AuthGUI;
import io.kamax.grid.soler.gui.fx.auth.login.LoginUIAuthSelectorPane;
import io.kamax.grid.soler.gui.fx.auth.login.ServerSelectionPane;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UncheckedIOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;
import java.util.function.Consumer;

public class LoginController {

    public interface AuthHandler {

        Pane getUI(Consumer<JsonObject> input);

    }

    private Logger log = LoggerFactory.getLogger(LoginController.class);

    private State state;
    private Stage stage;
    private SolerClient model;
    private AuthGUI gui;
    private Runnable after;

    private Map<String, AuthHandler> handlers;

    public static void run(State state, Stage stage, SolerClient model, AuthGUI gui, Runnable after) {
        LoginController c = new LoginController(state, stage, model, gui, after);
        c.show();
    }

    public LoginController(State state, Stage stage, SolerClient model, AuthGUI gui, Runnable after) {
        this.state = state;
        this.stage = stage;
        this.model = model;
        this.gui = gui;
        this.after = after;
        this.handlers = new HashMap<>();

        handlers.put("g.auth.id.password" + "g.id.local.username", new AuthHandler() {
            @Override
            public Pane getUI(Consumer<JsonObject> input) {
                JFXButton send = new JFXButton("Send");
                JFXTextField login = new JFXTextField();
                login.setOnAction(e -> send.fire());
                JFXPasswordField pass = new JFXPasswordField();
                pass.setOnAction(e -> send.fire());

                send.setOnAction(o -> {
                    JsonObject id = new JsonObject();
                    id.addProperty("type", "g.id.local.username");
                    id.addProperty("value", login.getText());
                    JsonObject data = new JsonObject();
                    data.addProperty("type", "g.auth.id.password");
                    data.add("identifier", id);
                    data.addProperty("password", pass.getText());
                    input.accept(data);
                });

                FlowPane userPane = new FlowPane();
                userPane.getChildren().addAll(new Label("Username"), login);

                FlowPane passPane = new FlowPane();
                passPane.getChildren().addAll(new Label("Password"), pass);


                VBox panel = new VBox();
                panel.getChildren().addAll(userPane, passPane, send);

                return panel;
            }
        });
    }

    public void show() {
        ServerSelectionPane pane = gui.getServerSelection();

        String defaultInput = System.getenv("SOLER_DEFAULT_SERVER_DATA_URI");
        if (StringUtils.isNotBlank(defaultInput)) {
            pane.setInput(defaultInput);
        }

        pane.addListener(ev -> {
            pane.clearError();
            String input = pane.getInput();
            ForkJoinPool.commonPool().invoke(new RecursiveAction() {

                @Override
                protected void compute() {
                    try {
                        SolerEndpoint endpoint;
                        try {
                            URL identityUrl = new URL(input);
                            endpoint = new SolerHttpEndpoint().setIdentity(identityUrl.toURI());
                        } catch (MalformedURLException e) {
                            Optional<SolerEndpoint> endpoints = model.discover(pane.getInput());
                            if (endpoints.isEmpty()) {
                                // FIXME better UI
                                throw new RuntimeException("No auto-discovery data available.");
                            }

                            endpoint = endpoints.get();
                        } catch (URISyntaxException e) {
                            // FIXME handle this better
                            // This shouldn't happen unless really bad stuff, need to dig how this could happen...
                            throw new RuntimeException(e);
                        }

                        SolerIdentityClient is = model.createIdentity(endpoint);
                        log.info("We got IS!");
                        UIAuthJson data = is.initLogin();
                        process(is, data);
                    } catch (UncheckedIOException e) { // FIXME show useful UI
                        log.warn("Error", e);
                        pane.showError("Unable to connect to the server - please validate");
                    } catch (RuntimeException e) { // FIXME show useful UI
                        log.warn("Unexpected error", e);
                        pane.showError("Unknown error. Check logs");
                    }
                }
            });
        });

        stage.setScene(new Scene(pane));
    }

    private void process(SolerIdentityClient client, UIAuthJson session) {
        if (session.isComplete()) {
            log.info("Session ID {}: Login succeeded", session.getSession());
            String accessToken = client.login(session.getSession());
            log.debug("Access token: {}", accessToken);
            state.setIsClient(client);
            log.info("Running post-login code");
            after.run();
            return;
        }

        LoginChoices c = new LoginChoices();
        if (session.getStep().getChoices().contains("g.auth.id.password")) {
            JsonObject parms = session.getParams().getOrDefault("g.auth.id.password", new JsonObject());
            List<String> types = GsonUtil.findArray(parms, "types").map(v -> GsonUtil.asList(v, String.class)).orElseGet(ArrayList::new);
            LoginChoices.Category cat = new LoginChoices.Category();
            cat.setLabel("Password");
            types.forEach(t -> {
                LoginChoices.Item i = new LoginChoices.Item();
                i.setId(t);
                i.setCatId("g.auth.id.password");
                i.setLabel(t);
                cat.getItems().add(i);
            });
            c.getCategories().add(cat);
        }

        LoginUIAuthSelectorPane authPane = new LoginUIAuthSelectorPane();
        authPane.init(c, item -> {
            log.info("Authentication selected: {} - {}", item.getCatId(), item.getId());
            String id = item.getCatId() + item.getId();
            AuthHandler p = handlers.get(id);
            if (Objects.isNull(p)) {
                JFXAlert alert = new JFXAlert();
                alert.setContent(new Label("Not supported"));
                alert.showAndWait();
            } else {
                stage.setScene(new Scene(p.getUI(i -> {
                    authPane.setDisable(true);
                    i.addProperty("session", session.getSession());

                    ForkJoinPool.commonPool().execute(() -> {
                        try {
                            process(client, client.auth(i));
                        } catch (RuntimeException e) {
                            e.printStackTrace();
                        } finally {
                            Platform.runLater(() -> authPane.setDisable(false));
                        }
                    });
                })));
            }
        });

        Platform.runLater(() -> stage.setScene(new Scene(authPane)));
    }

}
